terraform {
  backend "s3" {
    bucket = "my-backend-9"
    key    = "tf/terraform.tfstate"
    region = "us-east-1"
  }
}
