module "vpc1" {
    source = "./modules/iaac"

    identifier = "tg-devops-9am"
    vpc_cidr = "10.0.0.0/16"
    CreatedBy = "Ajay"
    public_subnet_az = "us-east-1a"
    private_subnet_az = "us-east-1b"
    public_subnet_cidr = "10.0.0.0/24"
    private_subnet_cidr = "10.0.1.0/24"
}
module "vpc2" {
    source = "./modules/iaac"

    identifier = "tg-devops-10am"
    vpc_cidr = "192.168.0.0/16"
    CreatedBy = "Ajinkya"
    public_subnet_az = "us-east-1a"
    private_subnet_az = "us-east-1b"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_cidr = "192.168.1.0/24"
}
module "vpc3" {
    source = "./modules/iaac"

    identifier = "tg-devops-11am"
    vpc_cidr = "192.168.0.0/16"
    CreatedBy = "kunal"
    public_subnet_az = "us-east-1a"
    private_subnet_az = "us-east-1b"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_cidr = "192.168.1.0/24"
}

    
 
