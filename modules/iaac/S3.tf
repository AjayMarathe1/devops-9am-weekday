resource "aws_s3_bucket" "tg-devops-bucket" {
  bucket = "${var.identifier}-bucket"
  
object_lock_enabled = false

  tags = {
    "name" = "${var.identifier}-bucket",
    "created by" = var.CreatedBy

  }
}

resource "aws_s3_bucket" "tg-devops-bucket-us" {
  provider = aws.ap
  bucket = "${var.identifier}-bucket-us"
  object_lock_enabled = false
  tags = {
    "name" = "${var.identifier}-bucket-us",
    "created by" = var.CreatedBy

  }

}